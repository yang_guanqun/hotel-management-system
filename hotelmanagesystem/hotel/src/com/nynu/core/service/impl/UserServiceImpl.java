package com.nynu.core.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nynu.core.dao.UserDao;
import com.nynu.core.po.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
 @Autowired
 private UserDao userDao;
 @Override
 public User findUser (String usercode,String password){
	 User user=this.userDao.findUser(usercode,password);
	 return user;
 }
}
