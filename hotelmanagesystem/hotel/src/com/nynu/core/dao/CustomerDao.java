package com.nynu.core.dao;
import java.util.List;
import com.nynu.core.po.Customer;

public interface CustomerDao {
	//Customer接口
	public List<Customer> selectCustomerList(Customer customer);
	// 客户列表
	public Integer selectCustomerListCount(Customer customer);
	//客户数
	
	public int createCustomer(Customer customer);
	
	public Customer getCustomerById(Integer id);
	// 鏇存柊瀹㈡埛淇℃伅
	public int updateCustomer(Customer customer);
	// 鍒犻櫎瀹㈡埛
	int deleteCustomer (Integer id);

}
